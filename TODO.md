TODO
====

- OPML support to retrieve URLs from multiple feeds.
- Support for other fetchers like wget, youtube-dl, you-get, quvi and git.
- Scuttler:
  - Cleanup content no longer pointed in scuttle database.
  - Support to generate only an index of all URLs.
  - Support to retrieve posts using the API.
